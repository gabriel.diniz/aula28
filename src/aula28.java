import java.util.Scanner;

public class aula28 { // do while II  - calculadora III

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);  // leitura

        int x = 0;  // variavel que armazena a operação a ser realizada
        double num1, num2, resultado;  // variaveis doubles, valores digitados e resultado

        do { // realiza a operação
            num1 = num2 = resultado = 0;

            System.out.println("Calculadora"); // imprime a mensagem
            System.out.println(" 1) somar "); // imprime a mensagem
            System.out.println(" 2) subtrair "); // imprime a mensagem
            System.out.println(" 3) multiplicar "); // imprime a mensagem
            System.out.println(" 4) dividir "); // imprime a mensagem
            System.out.println(" 0) Sair "); // imprime a mensagem

            System.out.println("Digite o código da operação que você deseja realizar"); // imprime a mensagem
            x = in.nextInt(); // armazena em x o valor digitado pelo usuario

            if (x != 0) { //se x for diferente de 0, verifica se o usuario deseja sair do programa

                System.out.println("Digite o primeiro número: ");
                num1 = in.nextDouble();

                System.out.println("Digite o segundo número: ");
                num2 = in.nextDouble();

                if (x == 1) { //soma x = 1
                    resultado = num1 + num2; // resultado igual a numero 1 somado com numero 2
                } else {  //se não
                    if (x == 2) { //subtrair x = 2
                        resultado = num1 - num2; // resultado igual a numero 1 subtraido por numero 2
                    } else { //se não
                        if (x == 3) { // multiplicar x = 3
                            resultado = num1 * num2; // resultado igual a numero 1 multiplicado por numero 2
                        } else { //se não
                            if (x == 4) { // dividir x = 4
                                resultado = num1 / num2;  // resultado igual a numero 1 dividido por numero 2
                            }
                        }
                    }
                }
                System.out.println( ); // linha em branco
                System.out.println("O resultado é: " + resultado);  // imprimi o resultado
                System.out.println( ); // linha em branco
            }
        } while (x != 0); // x é diferente de 0? se não, volta para o DO executa novamente
    }
}